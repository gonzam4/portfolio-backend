<!DOCTYPE html>
<html
  lang="es"
  xmlns="http://www.w3.org/1999/xhtml"
  xmlns:o="urn:schemas-microsoft-com:office:office"
>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta name="x-apple-disable-message-reformatting" />
    <title></title>
    <!--[if mso]>
      <style>
        table {
          border-collapse: collapse;
          border-spacing: 0;
          border: none;
          margin: 0;
        }
        div,
        td {
          padding: 0;
        }
        div {
          margin: 0 !important;
        }
      </style>
      <noscript>
        <xml>
          <o:OfficeDocumentSettings>
            <o:PixelsPerInch>96</o:PixelsPerInch>
          </o:OfficeDocumentSettings>
        </xml>
      </noscript>
    <![endif]-->
    <style>
      table,
      td,
      div,
      h1,
      p {
        font-family: Arial, sans-serif;
      }
      @media screen and (max-width: 530px) {
        .unsub {
          display: block;
          padding: 8px;
          margin-top: 14px;
          border-radius: 6px;
          background-color: #555555;
          text-decoration: none !important;
          font-weight: bold;
        }
        .col-lge {
          max-width: 100% !important;
        }
      }
      @media screen and (min-width: 531px) {
        .col-sml {
          max-width: 27% !important;
        }
        .col-lge {
          max-width: 73% !important;
        }
      }
    </style>
  </head>
  <body
    style="
      margin: 0;
      padding: 0;
      word-spacing: normal;
      background-color: #2e3440;
    "
  >
    <div
      role="article"
      aria-roledescription="email"
      lang="en"
      style="
        text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
        background-color: #2e3440;
      "
    >
      <table
        role="presentation"
        style="width: 100%; border: none; border-spacing: 0"
      >
        <tr>
          <td align="center" style="padding: 0">
            <!--[if mso]>
          <table role="presentation" align="center" style="width:600px;">
          <tr>
          <td>
          <![endif]-->
            <table
              role="presentation"
              style="
                width: 94%;
                max-width: 600px;
                border: none;
                border-spacing: 0;
                text-align: left;
                font-family: Arial, sans-serif;
                font-size: 16px;
                line-height: 22px;
                color: #363636;
              "
            >
              <tr>
                <td
                  style="
                    padding: 40px 30px 30px 30px;
                    text-align: center;
                    font-size: 24px;
                    font-weight: bold;
                  "
                >
                  <a
                    href="https://www.dowar.xyz/"
                    style="text-decoration: none"
                    ><img
                      src="https://dowar.xyz/assets/dowar.c8a9c92f.png"
                      width="400"
                      alt="Logo"
                      style="
                        width: 400px;
                        max-width: 80%;
                        height: auto;
                        border: none;
                        text-decoration: none;
                        color: #ffffff;
                      "
                  /></a>
                </td>
              </tr>
              <tr>
                <td style="padding: 30px; background-color: #ffffff">
                  <h1
                    style="
                      margin-top: 0;
                      margin-bottom: 16px;
                      font-size: 26px;
                      line-height: 32px;
                      font-weight: bold;
                      letter-spacing: -0.02em;
                    "
                  >
                    Petición de contacto recibida
                  </h1>
                  <p style="margin: 0">
                    Hola {{$name}}, gracias por ponerte en contacto, te responderé tan pronto me sea posible. 
                    <br />
                    Si no te respondo en un máximo de 48 h favor de reenviar el mensaje o utilizar otro medio de contacto. 
                    <br />
                    Si crees que esto se trata de un error te agradecería que me lo indicaras respondiendo a este correo.
                    <br />
                    Feliz día te desea Dowar.
                  </p>
                </td>
              </tr>
              <tr>
                <td
                  style="
                    padding: 30px;
                    text-align: center;
                    font-size: 12px;
                    background-color: #282c34;
                    color: #cccccc;
                  "
                >
                  <p style="margin: 0; font-size: 14px; line-height: 20px">
                    <a
                      href="https://dowar.xyz"
                      style="text-decoration: none; color: #81a1c1"
                      >DowarDev </a
                    >2022
                  </p>
                </td>
              </tr>
            </table>
            <!--[if mso]>
          </td>
          </tr>
          </table>
          <![endif]-->
          </td>
        </tr>
      </table>
    </div>
  </body>
</html>
